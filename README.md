# documentation

General documentation for tools that I use

The motive behind this is that I often use a variety of technology and sporadically document how to use them

This leads to specific information being dumped in note taking apps such as EverNote that become increasingly tedious to parse

Instead, why not document everything in Markdown and keep these docs next to the code I frequently write?
